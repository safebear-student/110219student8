package com.safebear.auto.tests;


import com.safebear.auto.tests.pages.LoginPage;
import com.safebear.auto.tests.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class StepDefs {

    // Stores the driver (e.g. ChromeDriver)
    static WebDriver driver  = Utils.getDriver();

    // Stores our page objects
    LoginPage loginPage = new LoginPage(driver);
    ToolsPage toolsPage= new ToolsPage(driver);

    @Before
    public void setUp(){

//        // Gets the driver (e.g. ChromeDriver)
//        driver = Utils.getDriver();
//
//        // Passes through the driver to our page objects
//        loginPage = new LoginPage(driver);
//        toolsPage = new ToolsPage(driver);
    }

    @After
    public void tearDown(){

        // This pauses the test at the end for 2 seconds
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleeps", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Given("I navigate to the login page")
    public void i_navigate_to_the_login_page() {

        // Step 1: ACTION: Open our web application in the browser
        driver.get(Utils.getUrl());

    }

    @When("I enter the login details for a {string}")
    public void i_enter_the_login_details_for_a(String userType) {

        switch(userType){
            case "invalidUser":
                // ACTION: Enter Username
                loginPage.inputUsername("blae");

                // ACTION: Enter Password
                loginPage.inputPassword("fsfdskjljfsl");

                // ACTION: Click on button
                loginPage.clickLoginButton();
                break;

            case "validUser":
                // ACTION: Enter Username
                loginPage.inputUsername("tester");

                // ACTION: Enter Password
                loginPage.inputPassword("letmein");

                // ACTION: Click on button
                loginPage.clickLoginButton();
                break;

            default:
                Assert.fail("The test data is wrong - the only accepted values are 'validUser' or 'invalidUser'. The data you passed was: " + userType);
        }

    }

    @Then("I can see the following message: {string}")
    public void i_can_see_the_following_message(String validationMessage) {

        switch (validationMessage){

            case "Username or Password is incorrect":

                Assert.assertTrue(loginPage.checkLoginFailedMessage().contains(validationMessage), "validation message is incorrect");
                break;

            case "Login Successful":

                Assert.assertTrue(toolsPage.checkLoginSuccessfulMessage().contains(validationMessage), "validation message is incorrect");
                break;

            default:
                Assert.fail("The test data is wrong. The data you passed was: " + validationMessage);
                break;

        }

    }


}
