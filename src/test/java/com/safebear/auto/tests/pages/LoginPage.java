package com.safebear.auto.tests.pages;

import com.safebear.auto.tests.pages.locators.LoginPageLocators;
import com.safebear.auto.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

// Will ensure that the LoginPage will ask for a 'driver'
@RequiredArgsConstructor
public class LoginPage {

    // Link our LoginPageLocators to this file
    LoginPageLocators locators = new LoginPageLocators();

    // Ensures that we have a 'driver' (e.g. ChromeDriver or FirefoxDriver)
    @NonNull
    WebDriver driver;

    // ACTION commands



    // Enter text into Username field
    public void inputUsername(String username){
        Utils.waitForElement(locators.getUsernameLocator(), driver).sendKeys(username);
    }

    // Enter text into Password field
    public void inputPassword(String password){
        Utils.waitForElement(locators.getPasswordLocator(), driver).sendKeys(password);
    }

    // Click on the Login button
    public void clickLoginButton(){
        Utils.waitForElement(locators.getButtonLocator(), driver).click();
    }

    // CHECK commands

    // Check the 'Login Failed' message appears
    public String checkLoginFailedMessage(){
        return Utils.waitForElement(locators.getWarningMessageLocator(), driver).getText();
    }

}
