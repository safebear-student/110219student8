package com.safebear.auto.tests.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

/**
 * This class contains locators for all the elements on the 'Login' page
 * <p>
 * The @Data tag creates a 'get' and a 'set' command
 * for each of my 'By' variables.
 * <p>
 * The 'get' command allows me to 'get' the value of these variables from another class
 * The 'set' command allows me to 'set' the value of these variables from another class
 */
@Data
public class LoginPageLocators {

    // Inputs
    private By usernameLocator = By.id("username");
    private By passwordLocator = By.xpath(".//input[@name='psw']");

    // Buttons
    private By buttonLocator = By.id("enter");

    // Messages
    private By warningMessageLocator = By.xpath("//p[@id='rejectLogin']/b");

}
