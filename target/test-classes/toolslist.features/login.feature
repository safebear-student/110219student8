Feature: Login

  Rules:
  1. The user must be warned if a login fails
  2. The user must be told if a login is successful

  @HighImpact
  @HighRisk
  Scenario Outline: Navigate and login to the application
    Given I navigate to the login page
    When  I enter the login details for a '<userType>'
    Then I can see the following message: '<validationMessage>'
    Examples:
      | userType    | validationMessage                 |
      | invalidUser | Username or Password is incorrect |
      | validUser   | Login Successful                  |